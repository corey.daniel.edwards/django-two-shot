from django.urls import path
from receipts.views import (
    ReceiptListView,
    AccountListView,
    ExpenseCategoryListView,
    ReceiptCreateView,
    AccountCreateView,
    ExpenseCategoryCreateView,
)

urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path("category/", ExpenseCategoryListView.as_view(), name="category_list"),
    path(
        "category/create",
        ExpenseCategoryCreateView.as_view(),
        name="category_create",
    ),
    path("account/create", AccountCreateView.as_view(), name="account_create"),
    path("create/", ReceiptCreateView.as_view(), name="receipt_create"),
]
